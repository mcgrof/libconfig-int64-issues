#include <stdio.h>
#include <stdint.h>
#include <stdlib.h>
#include <limits.h>
#include <libconfig.h>

int main(void)
{
	FILE *fp;
	char line[512];
	long long i, val, int_max_plus = INT_MAX;

	fp = fopen("int_max_plus_3.cfg", "w");

	fputs("example_options_0001:\n", fp);
	fputs("{\n", fp);
	for (i=0; i < INT_MAX; i++) {
		if (i + 1 == 0)
			break;
		val = i;
		snprintf(line, sizeof(line), "\tuint_val_%llu = %llu;\n", i, val);
		fputs(line, fp);
	}

	/* Add 3 values over INT_MAX */
	snprintf(line, sizeof(line), "\tuint_val_%llu = %llu;\n", int_max_plus, int_max_plus);
	fputs(line, fp);
	int_max_plus++;

	snprintf(line, sizeof(line), "\tuint_val_%llu = %llu;\n", int_max_plus, int_max_plus);
	fputs(line, fp);
	int_max_plus++;

	snprintf(line, sizeof(line), "\tuint_val_%llu = %llu;\n", int_max_plus, int_max_plus);
	fputs(line, fp);
	int_max_plus++;

	fputs("};\n", fp);

	return 0;
}
