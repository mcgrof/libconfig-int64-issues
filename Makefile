all: demo gen-large-config

LDFLAGS := $(shell pkg-config --libs libconfig)
CCFLAGS := $(shell pkg-config --cflags libconfig) -Wall

demo: example4.c example4-try-0001.cfg example4-try-0002.cfg
	gcc $(LDFLAGS) $(CCFLAGS) -o demo example4.c

gen-large-config: gen-large-config.c
	gcc $(CCFLAGS) -o gen-large-config gen-large-config.c

clean:
	rm -f demo *.o gen-large-config

distclean: clean
	rm -f int_max_plus_3.cfg

int_max_plus_3.cfg: gen-large-config
	./gen-large-config > int_max_plus_3.cfg

test-0001: demo
	./demo

test-0002: demo int_max_plus_3.cfg
	./demo
