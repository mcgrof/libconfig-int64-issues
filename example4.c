#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <string.h>
#include <errno.h>
#include <limits.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>

#include <libconfig.h>

static const char *config = "example4-try-0001.cfg";

/*
 * This example itereates over an example configuration line by line.
 * It also tests proper conversion for u64 values and how to deal with
 * automatic inference issues from libconfig when converting values.
 */

/*
 *
 * You have to be explicit with libconfig on expected values, and this
 * will be very program specific. Even if you are explicit libconfig can
 * still parse values beyond the limits.
 *
 * This exposes a few limitations with libconfig:
 *
 *   a) libconfig is limitd to INT_MAX number of settings / entries
 *   b) libconfig's automatic conversion to int can have adverse effects on
 *      your program unless you have taken some measures to deal with this.
 *   c) libconfig does automatic conversion by default so best to specify
 *      and expected types per entry
 *
 * To deal with a) this program demos how checks on settings / entries
 * can be considered.
 *
 * To deal with b) and c) this program demos how a program can use
 * long long for all numeric values. Note though how even if you stick
 * with long long there are 4 special cases you need to consider still:
 *
 *   I) Negative input values will be converted to respective unsigned int
 *   values. If you used automatic inference and CONFIG_TYPE_INT is used
 *   first to check against negative values you still would not be able
 *   to tell apart a user using an input values:
 *
 *           int_neg_3 = -3;
 *           uint_max_value_minus_2 = 4294967293;
 *
 *   Both of these get converted to -3 when CONFIG_TYPE_INT is used.
 *
 *   II) Using UINT_MAX + 1 is still accepted as input value and values get
 *      wrapped. Refer to uint_max_value_plus_1, uint_max_value_plus_2,
 *      uint_max_value_plus_3.
 *
 *   III) libconfig allows LONG_MAX, values after LONG_MAX will be kept pegged
 *   at LONG_MAX, values below LONG_MAX will correspond to the respective
 *   UINT_MAX - x value -- so:
 *
 *   	LONG_MAX - 2	--> UINT_MAX - 2
 *   	LONG_MAX - 1	--> UINT_MAX - 1
 *   	LONG_MAX	--> UINT_MAX
 *   	LONG_MAX +1	--> UINT_MAX
 *   	LONG_MAX +2	--> UINT_MAX
 *
 *   IV) "Special" values are currently parsed by libconfig - so assuming we
 *       are again forcing CONFIG_TYPE_INT64 for our digits (auto conversion
 *       might have even more issues):
 *
 *   	4294967295L		--> 4294967295 -- this is expected
 *   	4294967296L		--> 4294967296 -- this is *beyond* UINT_MAX
 *   	9223372036854775807L	--> 9223372036854775807 -- this is not expected
 *   	9223372036854775808L	--> 9223372036854775807 -- this is not expected
 *   	9223372036854775809L	--> 9223372036854775807 -- this is not expected
 */

static char *type_to_str(int type)
{
	switch (type) {
	case CONFIG_TYPE_INT:
		return "CONFIG_TYPE_INT";
	case CONFIG_TYPE_INT64:
		return "CONFIG_TYPE_INT64";
	case CONFIG_TYPE_FLOAT:
		return "CONFIG_TYPE_FLOAT";
	case CONFIG_TYPE_BOOL:
		return "CONFIG_TYPE_BOOL";
	case CONFIG_TYPE_STRING:
		return "CONFIG_TYPE_STRING";
	case CONFIG_TYPE_ARRAY:
		return "CONFIG_TYPE_ARRAY";
	case CONFIG_TYPE_LIST:
		return "CONFIG_TYPE_LIST";
	case CONFIG_TYPE_GROUP:
		return "CONFIG_TYPE_GROUP";
	default:
		return "Invalid";
	}
}

/*
 * Note, we loop with int, this also means libconfig is restricted to
 * INT_MAX entries given the limitations on config_setting_length() also
 * returning int.
 */
static int parse_config_setting(config_setting_t *setting,
				bool force_long_long,
				bool strict)
{
	int entries, i;
	int entry_type;
	config_setting_t *entry;
	const char *name;
	char *type_name;
	int var_int, var_bool;
	long long var_long_long;
	unsigned long long int var_u_long_long;
	double var_double;

	entries = config_setting_length(setting);
	if (!entries)
		return 0;

	printf("Number of entries: %d\n", entries);

	printf("%25s%20s%15s%20s%25s%25s%20s%20s\n",
	       "entry",
	       "entry_type",
	       "get_bool()",
	       "get_int()",
	       "get_int64()",
	       "get_int64(u)",
	       "get_float()",
	       "get_string()");

	for (i = 0; i <= entries; i++) {
		entry = config_setting_get_elem(setting, i);
		if (!entry)
			continue;

		entry_type = config_setting_type(entry);

		if (force_long_long && entry_type != CONFIG_TYPE_STRING)
			entry->type = CONFIG_TYPE_INT64;

		entry_type = config_setting_type(entry);

		var_int = 0;
		var_bool = 0;
		var_long_long = 0;
		var_u_long_long = 0;
		var_double = 0;
		name = NULL;

		type_name = type_to_str(entry_type);

		if (entry_type != CONFIG_TYPE_STRING) {
			var_int = config_setting_get_int(entry);
			var_bool = config_setting_get_bool(entry);
			var_long_long = config_setting_get_int64(entry);
			var_u_long_long = config_setting_get_int64(entry);
			var_double = config_setting_get_float(entry);

			/*
			 * libconfig accepts special characters and these can
			 * actually be over what we expect. Refer to:
			 *
			 *   o uint_special_max_plus_1
			 *   o long_special_max
			 *   o long_special_max_plus_1
			 *   o long_special_max_plus_2
			 */
			if (strict && var_long_long > UINT_MAX) {
				var_long_long = UINT_MAX;
				var_u_long_long = UINT_MAX;
			}

		} else if (entry_type == CONFIG_TYPE_STRING) {
			name =  config_setting_get_string(entry);
		}

		printf("%25s%20s%15d%20d%25llu%25llu%20f%20s\n",
		       config_setting_name(entry),
		       type_name,
		       var_bool,
		       var_int,
		       var_long_long,
		       var_u_long_long,
		       var_double,
		       name);
	}

	return 0;
}

#define BYTES_PER_ENTRY 100
static int check_file(const char *filename, unsigned long long max_entries)
{
	int fd;
	struct stat sb;
	int ret = 0;
	unsigned long long size_limit = 100 * max_entries;

	printf("-------- Limits --------------\n");
	/*
	 * Detect wrap around -- if max_entries * size_limit goves over
	 * unsigned long long we detect tha there.
	 */
	if (size_limit < max_entries) {
		fprintf(stderr,
			"Cannot do sanity check as limit is beyond limit\n");
		return -E2BIG;
	}

	if (max_entries > INT_MAX) {
		printf("Must be doing a stress test -- limit is beyond what libconfig undersands\n");
	}

	fd = open(filename, O_RDONLY);
	if (!fd)
		return -ENOENT;

	ret = fstat(fd, &sb);
	if (ret)
		goto out;

	printf("Max entries allowed %llu\n", max_entries);
	printf("File size limit: %llu bytes (~ %llu KiB, ~ %llu MiB, ~ %llu GiB\n",
	       size_limit,
	       size_limit / 1024,
	       size_limit / 1024 / 1024,
	       size_limit / 1024 / 1024 / 1024);

	printf("File size: %llu bytes (~ %llu KiB, ~ %llu MiB, ~ %llu GiB)\n",
	       (unsigned long long) sb.st_size,
	       (unsigned long long) sb.st_size / 1024,
	       (unsigned long long) sb.st_size / 1024 / 1024,
	       (unsigned long long) sb.st_size / 1024 / 1024 / 1024);

	if (sb.st_size > size_limit) {
		fprintf(stderr,
			"File %s is too big! Max entries expected: %llu\n",
			filename, max_entries);
		return -E2BIG;
	}

out:
	close(fd);

	return ret;
}
#undef BYTES_PER_ENTRY

static int parse_config(bool force_long_long,
			bool strict,
			bool stress_test,
			const char *alt_config)
{
	int ret = -ENOENT;
	config_t cfg;
	config_setting_t *root, *setting;
	int count, i, setting_type;
	const char *read_config;
	unsigned long long max_entries = 1000;

	if (alt_config)
		read_config = alt_config;
	else
		read_config = config;

	/*
	 * libconfig should support only INT_MAX, we enable UINT_MAX
	 * only if doing a stress test against libconfig.
	 */
	if (stress_test)
		max_entries = UINT_MAX;

	ret = check_file(read_config, max_entries);
	if (ret)
		return ret;

	printf("***********************************************************\n");
	printf("**** Force long long: %3s *********** Use strict: %3s *****\n",
	       force_long_long ? "YES" : "NO",
	       strict ? "YES" : "NO");
	printf("***********************************************************\n");

	config_init(&cfg);

	ret = config_read_file(&cfg, read_config);
	if (ret != CONFIG_TRUE) {
		fprintf(stderr, "%s:%d - %s\n",
			config_error_file(&cfg),
			config_error_line(&cfg),
			config_error_text(&cfg));
		ret = errno;
		goto out;
	}

	root = config_root_setting(&cfg);
	if (!root) {
		ret = -ENOENT;
		fprintf(stderr, "No root found\n");
		goto out;
	}

	count = config_setting_length(root);
	if (count > 2)
		return -EINVAL;

	printf("Number of settings: %d\n", count);

	for (i = 0; i <= count; i++) {
		setting = config_setting_get_elem(root, i);
		if (!setting)
			continue;
		/*
		 * We only support group type for the first level in the
		 * config tree.
		 */
		setting_type = config_setting_type(setting);
		if (setting_type != CONFIG_TYPE_GROUP)
			continue;

		printf("--------------------------------------------\n");
		printf("Setting name: %s\n", config_setting_name(setting));

		ret = parse_config_setting(setting, force_long_long, strict);
		if (ret)
			break;
	}

out:
	config_destroy(&cfg);

	return ret;
}

void test(void)
{
	long long test1 = UINT_MAX + 3;
	unsigned long long test2 = UINT_MAX + 3;
	unsigned int test3 = test1;

	long long test4 = 4294967298; /* the same */

	printf("test1: %llu\n", test1); /* comes out as 2 */
	printf("test2: %llu\n", test2); /* comes out as 2 */
	printf("test3: %u\n", test3); /* comes out as 2 */
	printf("test4: %llu\n", test4); /* comes out as itself ! */

	/* Upstream libconfig parser should do: */
	if (test4 > UINT_MAX) {
		/* This or grant the option to error out */
		test4 = UINT_MAX;
		printf("Detected wrap around on test4...\n");
		printf("test4: %llu\n", test4); /* fixed now */
	}
}

static void flush_all(void)
{
	fflush(stdout);
	fflush(stderr);
}

static int test_large_input(void)
{
	int ret;
	int libconfig_patched = 0;

	printf("This program uses your system limits.h on example4.cfg, if there\n");
	printf("is a mismatch with your limits try modifying %s test\n", config);
	printf("how libconfig reacts to your own system limits\n\n");

	printf("Your system limits.h says:\n\n");

	printf("INT_MAX: %d\n", INT_MAX);
	printf("UINT_MAX: %u\n", UINT_MAX);
	printf("LONG_MAX: %u\n\n", UINT_MAX);

	ret = parse_config(false, false, false, NULL);
	if (ret) {
		if (ret != -ERANGE) {
			fprintf(stderr, "Error on first try: %s\n", strerror(ret));
			flush_all();
			return(EXIT_FAILURE);
		} else {
			libconfig_patched++;
			printf("libconfig seems to be properly patched!\n");
		}
	}

	/*
	 * XXX:
	 *
	 * libconfig seems to need a CONFIG_TYPE_UINT64 as otherwise
	 * the parser will accept negative values even if you don't
	 * want them, and there is no way to detect this.
	 *
	 * In lieu of this, programs should use a practice like the strict
	 * setting in this program.
	 */
	ret = parse_config(true, false, false, "example4-try-0002.cfg");
	if (ret) {
		if (ret != -ERANGE) {
			fprintf(stderr, "Error on second try: %s\n", strerror(ret));
			flush_all();
			return(EXIT_FAILURE);
		} else {
			if (!libconfig_patched) {
				printf("libconfig may be properly patched!\n");
				printf("However.. the pass should have failed...\n");
			}
		}
	}

	ret = parse_config(true, true, false, "example4-try-0002.cfg");
	if (ret) {
		fprintf(stderr, "Error on last try: %s\n", strerror(ret));
		flush_all();
		return(EXIT_FAILURE);
	}

	/*
	 * Respective patch for libconfig is on the file
	 * fixes/0001-libconfig-check-against-uint-max-to-avoid-wrap-aroun.patch
	 */
	if (!libconfig_patched)
		test();

	printf("\ntest-0001 completed OK!\n");

	return 0;
}

static int test_large_num_entries(void)
{
	int ret;
	const char *test_file = "int_max_plus_3.cfg";

	ret = parse_config(false, false, true, test_file);
	if (ret) {
		if (ret == -ENOENT) {
			fprintf(stderr, "\ntest-0002 is optional, %s is missing so skipping\n", test_file);
			fprintf(stderr, "If you would like to run this test run: make test-0002\n\n");
			fprintf(stderr, "\tmake test-0002\n");
		} else
			return ret;
	}

	printf("\ntest-0002 completed OK!\n");

	return 0;
}

int main(int argc, char **argv)
{
	int ret;

	ret = test_large_input();
	if (ret)
		return ret;
	ret = test_large_num_entries();
	if (ret)
		return ret;

	return(EXIT_SUCCESS);
}
