Issues with libconfig with int64
================================

libconfig has a series of issues when supporting and parsing 64-bit integer
values. This document describes these issues and addresses best practices,
and counter measures one can take as a developer to prevent unexpected
program behaviour when using libconfig. Possible issues with existing
applications using libconfig are not evaluated, however it should be easy
to deduce what types of issues these might be from the issues described here.

Provided the non-standard careful best practices implemented to work around
libconfig issues in example4.c are followed libconfig 1.5 has been vetted for
use of 64-bit integer values provided one is willing to accept wrap around
issues. The wrap around issues can only be dealt with by patching libconfig,
a [patch](fixes/0001-libconfig-check-against-uint-max-to-avoid-wrap-aroun.patch)
for libconfig is provided under fixes/ to address this.

libconfig project pages
----------------------

  * http://www.hyperrealm.com/libconfig
  * https://github.com/hyperrealm/libconfig

No mailing list ?
No bugzilla ? Git issues I guess...

Default use of CONFIG_TYPE_INT
------------------------------

libconfig supports a few data types for input. One of them is CONFIG_TYPE_INT,
another is CONFIG_TYPE_INT64. By defalut libconfig will parse values as
CONFIG_TYPE_INT, so if you are using 64-bit values you need to set the type
explicitly. This is not documented anywhere. To be clear, if you just use
config_setting_get_int64_elem(setting, idx) you will not get an int64 value.

There can be adverse effects on programs due to this automatic prefence
and choice for CONFIG_TYPE_INT. To determine the effects one will need to
evaluate the specific program and how it uses values parsed. For a series of
test refer to the first test run on example4.c.

Cannot avoid negative value input
---------------------------------

libconfig defaults to CONFIG_TYPE_INT, but even if you set the type to
CONFIG_TYPE_INT64, you will still allow for negative input values.
The only way to fix this would be to add support for two new types:

  * CONFIG_TYPE_UINT
  * CONFIG_TYPE_UINT64

You can still use CONFIG_TYPE_INT64, and if you use the undocumented way to
force properly CONFIG_TYPE_INT64 -- there are still issues to consider which
the example4 program demos, assuming you want uint 64 bit values.

Wrap arounds not handled
------------------------

Using UINT_MAX + 1 are accepted as input value, when this is done values get
wrapped. Refer to uint_max_value_plus_1, uint_max_value_plus_2,
uint_max_value_plus_3 on example4.

Long max values accepted
------------------------

libconfig allows LONG_MAX, values after LONG_MAX will be kept pegged
at LONG_MAX, values below LONG_MAX will correspond to the respective
UINT_MAX - x value -- so:

  * LONG_MAX - 2 --> UINT_MAX - 2
  * LONG_MAX - 1 --> UINT_MAX - 1
  * LONG_MAX	 --> UINT_MAX
  * LONG_MAX +1	 --> UINT_MAX
  * LONG_MAX +2	 --> UINT_MAX

This can be fixed if the same wrap around issue is fixed.

Special values accepted
-----------------------

"Special" values are currently parsed by libconfig - so assuming we are
forcing CONFIG_TYPE_INT64 for our digits (auto conversion might have even
more issues):

  * 4294967295L		--> 4294967295 -- this is expected
  * 4294967296L		--> 4294967296 -- this is *beyond* UINT_MAX
  * 9223372036854775807L	--> 9223372036854775807 -- this is not expected
  * 9223372036854775808L	--> 9223372036854775807 -- this is not expected
  * 9223372036854775809L	--> 9223372036854775807 -- this is not expected

Limitations on number of entries
--------------------------------

libconfig is limitd to INT_MAX number of settings / entries. In *theory* this
_should_ only be a theoretical and practical issue if you might want to support
around INT_MAX entry values on your configuration value -- however testing
reveals that in practice libconfig will gobble up any file given and try to
parse it. If the file is large we are the whims of a full run of
config_read_file() to complete a test against the used configuration file.

There are two issues then, an implicit issue, and a practical issue still
being evaluated:

  * Implicit: config_read_file() can stall -- you need a file pre-check
  * Practical issue: its unclear what going over INT_MAX might mean

### Implicit: config_read_file() can stall -- you need a file pre-check

If systems rely on a userspace program which uses libconfig to parse a file
since libconfig start point on a file is with config_read_file() and this
includes a syntax error check, libconfig will iterate over all entries and
provide a sanity check on the entire file synchronously instead of first
evaluating potential size limits.

Programs which use libconfig then should have a sanity pre-file check added
which analyzes the file going to be parsed, at least for a file size check
to prevent any stalls.

### Practical issue: its unclear what going over INT_MAX might mean

libconfig programs are limited to INT_MAX entries as int is used.
It has not been tested yet what going beyond the INT_MAX limit number
of limits means to libconfig. It is not clear if this can have any
adverse effects other than the obvious stalls implicated by using
config_read_file() without a program specific pre file check.

We have added test to stress test this, note however that this test requires
between 60-70 GiB of space to spare and a lot of time to run. This test is left
out and is optional. To compile and run it:

	make test-0002

Tests are running to evaluate this at this time.

Fixes and TODO
==============

Wrap around issues
------------------

There is one patch which can help fix the wrap around issues:

[fixes/0001-libconfig-check-against-uint-max-to-avoid-wrap-aroun.patch](fixes/0001-libconfig-check-against-uint-max-to-avoid-wrap-aroun.patch)

The example4.c program has a test case for its application, -ERANGE
would be detected on errno if used on the first iteration of the program.

CONFIG_TYPE_UINT and CONFIG_TYPE_UINT64
---------------------------------------

Negative values cannot be addressed unless CONFIG_TYPE_UINT and
CONFIG_TYPE_UIN64 get added to libconfig. This work needs to be done.

Program specific fixes
-----------------------

Two issues are program specific:

  * Limits on number of entries
  * Parsing invalid values

### Limits on number of entries

libconfig should be modified to enable user input to allow to specify the
dimensions of a configuration, to not allow more than what can be expected.
Programs can also verify the number of entries per setting.

### Parsing invalid values

Programs may need to be studied independently to see how they deal with
the libconfig issues and if they provide their own sanity. This may need
studying on a case by case basis, or through an evaluation using
Coccinelle rules against programs: for instance looking for data types
used for values obtained through the libconfig API.

What can be done proactively
============================

The example4 program is specially crafted to handle unsigned int 64 bit values,
it allows a force mode to force int64 and also has a check against high values
which leaks on libconfig. Both mechanisms can be mimicked on programs in lieu
CONFIG_TYPE_UINT and CONFIG_TYPE_UINT64, however wrap around values will still
be accepted until libconfig is properly patched. The wrap around issue can only
be dealt by patching libconfig with the suggested patch under fixes/.
